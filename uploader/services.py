from django.conf import settings

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError

import logging

logger = logging.getLogger(__name__)

class DigivlaElasticArticle():
	def __init__(self):
		if not settings.DIGIVLA_ELASTIC_HOST or not settings.DIGIVLA_ELASTIC_PORT:
			raise Exception("ensure DIGIVLA_ELASTIC_HOST and DIGIVLA_ELASTIC_PORT set on settings")
		self.elastic_host = settings.DIGIVLA_ELASTIC_HOST
		self.elastic_port = settings.DIGIVLA_ELASTIC_PORT
		self.es = Elasticsearch([{'host':settings.DIGIVLA_ELASTIC_HOST,'port':settings.DIGIVLA_ELASTIC_PORT,'use_ssl':False},])

	def add_article(self, article):
		
		try:
			data =self.es.get(index='digivlaschema',doc_type="content", id=article.article_id)

		except NotFoundError as e:
			if article:
				body={  "article_id":str(article.article_id),
		                "title" : article.title,
	            		"media_id" : str(article.media_id),
		                "datee" : article.datee.strftime("%Y-%m-%d %H:%M:%S"),
	             		"content" : article.content,
				 		"mmcol" : str(article.mmcol),
				 		"circulation" : str(article.circulation),
				 		"page" : article.page,
				 		"file_pdf" : article.file_pdf,
				 		"columne" : str(article.columne),
		                "size_jpeg" : str(article.size_jpeg),
		                "journalist" : article.journalist,
		                "rate_bw" : str(article.rate_bw),
		                "rate_fc" : str(article.rate_fc),
		                "is_chart" : str(article.is_chart),
		                "is_table" : str(article.is_table),
		                "is_colour" : str(article.is_colour),
		              }
				print(body)
				response = self.es.index(index='digivlaschema', doc_type="content",id=article.article_id,body=body)
				print(response)
				
				logger.debug("article %s trasnfered to elastic " %article.title)
				logger.debug("%s" %(response))
			else:
				print("article is empty nothing to do")
				logger.info("article is empty nothing to do")

	def delete_article(self,article_id):
		try:
			es.delete(index='digivlaschema', doc_type="content",id=article_id)
		except Exception as e:
			msg="error when delete article on elastic with message %s " %str(e)
			print(msg)
			logger.info(msg)

	def update_article(self,article):
		if article:
			print("trying update article %s " %(article.article_id))
			body={ 'doc': { "article_id":str(article.article_id),
			                "title" : article.title,
		            		"media_id" : str(article.media_id),
			                "datee" : article.datee.strftime("%Y-%m-%d %H:%M:%S"),
		             		"content" : article.content,
					 		"mmcol" : str(article.mmcol),
					 		"circulation" : str(article.circulation),
					 		"page" : article.page,
			                "file_pdf" :article.file_pdf,
			                "columne" : str(article.columne),
			                "size_jpeg" : str(article.size_jpeg),
			                "journalist" : article.journalist,
			                "rate_bw" : str(article.rate_bw),
			                "rate_fc" : str(article.rate_fc),
			                "is_chart" : str(article.is_chart),
			                "is_table" : str(article.is_table),
			                "is_colour" : str(article.is_colour)
			              }
	             }
	        try:
	        	data =self.es.update(index='digivlaschema',doc_type="content", id=article.article_id,body=body)
	        except NotFoundError as e:
	        	msg = "data not found in elastic message : %s" %msg
	        	print(msg)
	        	logger.info(msg)

