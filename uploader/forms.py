from django import forms
from uploader.models import TbArticlesTv, TbArticlesRadio,TbArticles, TbArticles
from media_models.models import TbMedia
from django.contrib.auth.forms import AuthenticationForm 
class ArticleTvForm(forms.ModelForm):
	media_id=forms.IntegerField(required=True)
	title=forms.CharField(required=True,max_length=150)
	class Meta:
		exclude=[]
		model= TbArticlesTv

class RadioTvForm(forms.ModelForm):
	class Meta:
		exclude=[]
		model = TbArticlesRadio

class LoginForm(AuthenticationForm):
	username= forms.CharField(required=True, max_length=100)
	password = forms.CharField(required=True, max_length=100)


class ArticleOnlineForm(forms.ModelForm):
	media_id = forms.IntegerField(required=True)
	title = forms.CharField(required=True)
	content = forms.CharField(required=True)
	class Meta:
		exclude=['columnee','size_jpeg','rate_bw','rate_fc','is_chart','is_table','is_colour','circulation','columne','article_id']
		model = TbArticles

class MediaForm(forms.ModelForm):
	class Meta:
		exclude=['pc_name','usere','input_date',]
		model=TbMedia