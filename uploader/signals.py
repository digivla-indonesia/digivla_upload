from django.db.models.signals import  post_delete,post_save
from django.dispatch import receiver
from django.conf import settings

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError


from uploader.models import TbArticlesTv, TbArticles
from django.db import  transaction

import os, errno
import logging

logger = logging.getLogger(__name__)




@receiver(post_delete, sender=TbArticlesTv)
def on_articletv_deleted(sender, instance, **kwargs):

	delete_file =  os.path.join(settings.MEDIA_ROOT,instance.filee.name)
	try:
		os.remove(delete_file)
		print("======delete file")
		print(delete_file)
		print(instance.filee)
	except OSError as e:
		pass
		# if e.errno != errno.ENOENT:
		# 	raise 
	try:
		es = Elasticsearch([{'host':settings.DIGIVLA_ELASTIC_HOST,'port':settings.DIGIVLA_ELASTIC_PORT,'use_ssl':False},])
		es.delete(index='digivlaschema', doc_type="content",id=instance.article_id)
	except Exception as e:
		print("error delete elastic message : %s" %str(e))
	

# @receiver(post_save, sender=TbArticlesTv)
# def on_articletv_saved(sender, instance, **kwargs):
# 	print("articletv saved, running elastic saving------ article id :%s" %instance.article_id)
	
# 	if not settings.DIGIVLA_ELASTIC_HOST or not settings.DIGIVLA_ELASTIC_PORT:
# 		logger.error("no DIGIVLA_ELASTIC_HOST or DIGIVLA_ELASTIC_PORT found in settings please check settings file")
# 	else:
# 		es = Elasticsearch([{'host':settings.DIGIVLA_ELASTIC_HOST,'port':settings.DIGIVLA_ELASTIC_PORT,'use_ssl':False},])
# 		try:
# 			data =es.get(index='digivlaschema',doc_type="content", id=instance.article_id)
# 			logger.info("elastic for article_id %s was exist insert elastic cancel" %(instance.article_id))

# 		except NotFoundError as e:
# 			article = TbArticles.objects.filter(pk=instance.article_id).first()
# 			if article:
# 				body={  "article_id":str(article.article_id),
# 		                "title" : article.title,
# 	            		"media_id" : str(instance.media_id),
# 		                "datee" : article.datee.strftime("%Y-%m-%d %H:%M:%S"),
# 	             		"content" : article.content,
# 				 		"mmcol" : str(article.mmcol),
# 				 		"circulation" : str(article.circulation),
# 				 		"page" : article.page,
# 				 		"file_pdf" : instance.filee.name.split('/')[-1],
# 				 		"columne" : str(article.columne),
# 		                "size_jpeg" : str(article.size_jpeg),
# 		                "journalist" : article.journalist,
# 		                "rate_bw" : str(article.rate_bw),
# 		                "rate_fc" : str(article.rate_fc),
# 		                "is_chart" : str(article.is_chart),
# 		                "is_table" : str(article.is_table),
# 		                "is_colour" : str(article.is_colour),
# 		              }
				
# 				response = es.index(index='digivlaschema', doc_type="content",id=article.article_id,body=body)
# 				print(response)
# 				print(body)
# 				logger.debug("article %s trasnfered to elastic " %article.title)
# 				logger.debug("%s" %(response))
# 			else:
# 				print("elastic trying save but no article found for article_id : %s" %instance.article_id)


# @receiver(post_save, sender=TbArticles)
# def on_article_saved(sender,instance, **kwargs):
# 	print("article saved, running elastic saving------")
# 	print(sender)
	
# 	if not settings.DIGIVLA_ELASTIC_HOST or not settings.DIGIVLA_ELASTIC_PORT:
# 		logger.error("no DIGIVLA_ELASTIC_HOST or DIGIVLA_ELASTIC_PORT found in settings please check settings file")
# 	else:
# 		es = Elasticsearch([{'host':settings.DIGIVLA_ELASTIC_HOST,'port':settings.DIGIVLA_ELASTIC_PORT,'use_ssl':False},])
# 		try:
# 			data =es.get(index='digivlaschema',doc_type="content", id=instance.article_id)
# 			logger.info("elastic for article_id %s was exist insert elastic cancel" %(instance.article_id))

# 		except NotFoundError as e:
			
		
# 			body={  "article_id":str(instance.article_id),
# 	                "title" : instance.title,
#             		"media_id" : str(instance.media_id),
# 	                "datee" : instance.datee.strftime("%Y-%m-%d %H:%M:%S"),
#              		"content" : instance.content,
# 			 		"mmcol" : str(instance.mmcol),
# 			 		"circulation" : str(instance.circulation),
# 			 		"page" : instance.page,
# 	                "file_pdf" : instance.file_pdf,
# 	                "columne" : str(instance.columne),
# 	                "size_jpeg" : str(instance.size_jpeg),
# 	                "journalist" : instance.journalist,
# 	                "rate_bw" : str(instance.rate_bw),
# 	                "rate_fc" : str(instance.rate_fc),
# 	                "is_chart" : str(instance.is_chart),
# 	                "is_table" : str(instance.is_table),
# 	                "is_colour" : str(instance.is_colour),
# 	              }

# 			response = es.index(index='digivlaschema', doc_type="content",id=instance.article_id,body=body)
# 			print(response)
# 			logger.debug("instance %s trasnfered to elastic " %instance.title)
# 			logger.debug("%s" %(response))
			
