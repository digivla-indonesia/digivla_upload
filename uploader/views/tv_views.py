from django.shortcuts import render
from uploader.models import  TbArticlesTv, TbArticles
from media_models.models import TbMediaType, TbMedia
from django.core.paginator import  Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django import http
from django.contrib.auth.decorators import login_required
from django.db import  transaction
from django.db.models import Q
from django.conf import settings 
from django.core.urlresolvers import reverse

from uploader.forms import ArticleTvForm, LoginForm, ArticleOnlineForm, MediaForm
from uploader.services import DigivlaElasticArticle

@login_required(login_url='/login/')
def index(request):
	return render(request, 'uploader/index.html')


@login_required(login_url='/login/')
def upload_tv(request, instance=None):
	status=200
	if request.method == 'POST':
		form = ArticleTvForm(request.POST, request.FILES, instance=instance)
		if form.is_valid():
			try:
				with transaction.atomic():
					instance = form.save(commit=False)
					media = TbMedia.objects.get(pk=instance.media_id)
					article = TbArticles()
					article.title =instance.title
					article.media_id = instance.media_id
					article.datee = instance.datee
					article.content = instance.content
					article.mmcol = 0
					article.circulation = media.circulation
					article.page ='0'
					# article.file_pdf=instance.filee.name
					article.columne=0
					article.size_jpeg=0
					article.journalist = instance.journalist
					article.rate_bw = media.rate_bw
					article.rate_fc = media.rate_fc
					article.is_chart =0
					article.is_table =0
					article.is_colour=0
					article.save()
					instance.article_id=article.article_id
					instance.save()
					short_file_name = instance.filee.name.split('/')[-1]
					article.file_pdf = short_file_name
					article.save()
					elastic = DigivlaElasticArticle()
					elastic.add_article(article)

					messages.success(request, u'{} uploaded'.format(instance))
					return http.HttpResponseRedirect(reverse('list-tv'))
			except Exception as e:
				# raise e
				print("error %s" %str(e))
				status=422

		else:
			status = 422
	else:
		form = ArticleTvForm(instance=instance)

    # return shortcuts.render(request, 'package_form.html', {
    #     'form':form
    # }, status=status)
  	medias = TbMedia.objects.filter(media_type_id=12, statuse='A')
  	if settings.NUMBER_UPLOAD_FORM:
  		form_numbers = range(1,settings.NUMBER_UPLOAD_FORM)
  	else:
  		form_numbers=range(1,6)
  	return render(request, 'uploader/upload-tv.html',{'form':form, 'medias':medias,'form_numbers':form_numbers}, status=status)


@login_required(login_url='/login/')
def edit_tv(request, pk):
 	instance = TbArticlesTv.objects.get(pk =pk)
	if request.method == 'POST':
		article_id=instance.article_id
		form = ArticleTvForm(request.POST, instance=instance)
		if form.is_valid():
			tv=form.save(commit=False)
			tv.article_id=article_id
			media = TbMedia.objects.get(pk=instance.media_id)
			article = TbArticles.objects.get(pk=article_id)
			article.title =instance.title
			article.media_id = instance.media_id
			article.datee = instance.datee
			article.content = instance.content
			
			article.circulation = media.circulation
		
			article.journalist = instance.journalist
			article.rate_bw = media.rate_bw
			article.rate_fc = media.rate_fc
		
			article.save()
			tv.save()
			# article=TbArticles.object.get(tv.article_id)
			elastic = DigivlaElasticArticle()
			elastic.update_article(article)

			return http.HttpResponseRedirect(reverse('list-tv'))
	else:		
		
		form = ArticleTvForm(instance=instance)
	medias = TbMedia.objects.filter(media_type_id=12, statuse='A')
	return render(request, 'uploader/edit-tv.html', {'form':form,'medias':medias})

@login_required(login_url='/login/')
def list_tv(request):
	tv_list = TbArticlesTv.objects.all().order_by('-createat')
	page = request.GET.get('page', 1)
	page=int(page)
	term = request.GET.get('term',None)
	sdate = request.GET.get('sdate',None)
	edate = request.GET.get('edate',None)
	media_id = request.GET.get('media_id',None)
	qs=''
	if term:
		tv_id = 0
		
		try:
			tv_id=int(term)
		except Exception as e:
			tv_id=0

		if tv_id > 0:
			tv_list = TbArticlesTv.objects.filter(Q(pk=tv_id) | Q(article_id=tv_id))
			
		elif sdate and edate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__gte=sdate, datee__lte=edate,media_id=media_id).order_by('-pk')
				qs = "&term=%s&sdate=%s&edate=%s&media_id=%s" %(term,sdate,edate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__gte=sdate, datee__lte=edate).order_by('-pk')
				qs = "&term=%s&sdate=%s&edate=%s" %(term,sdate,edate)
			print("sdate and edate")
		elif sdate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__gte=sdate, media_id=media_id).order_by('-pk')
				qs = "&term=%s&sdate=%s&media_id=%s" %(term,sdate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__gte=sdate).order_by('-pk')
				qs = "&term=%s&sdate=%s" %(term,sdate)
			print("sdate")
		elif edate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__lte=edate, media_id=media_id).order_by('-pk') 
				qs = "&term=%s&edate=%s&media_id=%s" %(term,edate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), datee__lte=edate).order_by('-pk') 
				qs = "&term=%s&edate=%s" %(term,edate)
		else:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term), media_id=media_id).order_by('-pk') 
				qs = "&term=%s&media_id=%s" %(term, media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(Q(title__icontains=term)|Q(content__icontains=term)).order_by('-pk') 
				qs = "&term=%s" %term

	else:
		
		if sdate and edate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(datee__gte=sdate, datee__lte=edate,media_id=media_id).order_by('-pk') 
				qs = "&sdate=%s&edate=%s&media_id=%s" %(sdate,edate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(datee__gte=sdate, datee__lte=edate).order_by('-pk') 
				qs = "&sdate=%s&edate=%s" %(sdate,edate)
		elif sdate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(datee__gte=sdate, media_id=media_id).order_by('-pk') 
				qs = "&sdate=%s&media_id=%s" %(sdate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(datee__gte=sdate).order_by('-pk') 
				qs = "&sdate=%s" %(term,sdate)
		elif edate:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(datee__lte=edate, media_id=media_id).order_by('-pk') 
				qs = "&edate=%s&media_id=%s" %(edate,media_id)
			else:
				tv_list =TbArticlesTv.objects.filter(datee__lte=edate).order_by('-pk') 
				qs = "&edate=%s" %(edate)
		else:
			if media_id:
				tv_list =TbArticlesTv.objects.filter(media_id=media_id).order_by('-pk') 
				qs = "&media_id=%s" %(media_id)
			else:
				# raise Exception("you must provider term or filter item")
				tv_list = TbArticlesTv.objects.all().order_by('-pk')
		# article_list =TbArticles.objects.filter(Q(title__icontains=term)|Q(content__icontains=term)).order_by('-pk') 
		# qs = "&term=%s" %term
		# else:
		# article_list = TbArticles.objects.all().order_by('-pk') 

	paginator = Paginator(tv_list,10)
	try:
		tvs = paginator.page(page)
	except PageNotAnInteger:
		tvs=paginator.page(1)
	except EmptyPage:
		tvs = paginator.page(paginator.num_pages)

	medias = TbMedia.objects.filter(media_type_id=12, statuse='A')
	return render(request, 'uploader/list-tv.html',{'tvs':tvs,'medias':medias,'qs':qs})

@login_required(login_url='/login/')
def delete_tv_confirmation(request, pk):
	tv = TbArticlesTv.objects.filter(pk=pk).first()
	return render(request, 'uploader/delete-confirmation.html',{'cancel_link':reverse('list-tv'),'ok_link':reverse('delete-tv',kwargs={'tv_id':pk}), 'object':tv})
@login_required(login_url='/login/')
def delete_tv(request,tv_id):
	with transaction.atomic():
		tv = TbArticlesTv.objects.filter(id=tv_id).first()
		if tv.article_id:
			article = TbArticles.objects.filter(pk=tv.article_id).first()
			if article:
				article.delete()
		tv.delete()

	messages.info(request, 'Deleted {}'.format(tv))
	return http.HttpResponseRedirect(reverse('list-tv'))