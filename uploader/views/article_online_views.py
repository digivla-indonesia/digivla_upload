from django.shortcuts import render
from uploader.models import  TbArticlesTv, TbArticles
from media_models.models import TbMedia, TbMediaType
from django.core.paginator import  Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django import http
from django.contrib.auth.decorators import login_required
from django.db import  transaction
from django.db.models import Q
from django.conf import settings 
from django.core.urlresolvers import reverse

from uploader.forms import ArticleTvForm, LoginForm, ArticleOnlineForm, MediaForm

from uploader.services import DigivlaElasticArticle

@login_required(login_url='/login/')
def article_detail(request, article_id):
	article  = TbArticles.objects.get(pk = article_id)
	media = TbMedia.objects.get(pk = article.media_id)
	tv = TbArticlesTv.objects.filter(article_id=article_id).first()
	return render(request, 'uploader/detail-article.html',{'article':article,'media':media,'tv':tv})


# region input news manual
@login_required(login_url='/login/')
def add_online_news(request, instance=None):
	if request.method == 'POST':
		form = ArticleOnlineForm(request.POST, instance=instance)
		if form.is_valid():
			article=form.save(commit=False)
			media = TbMedia.objects.get(pk = article.media_id)
			article.circulation = media.circulation
			article.rate_bw = media.rate_bw
			article.rate_fc = media.rate_fc
			article.is_chart =0
			article.is_table =0
			article.is_colour=0
			article.size_jpeg=0
			article.columne =0
			article.save()
			elastic = DigivlaElasticArticle()
			elastic.add_article(article)
			return http.HttpResponseRedirect(reverse('list-online-news'))
	else:
		form = ArticleOnlineForm(instance=instance)
	medias = TbMedia.objects.filter(media_type_id=4, statuse='A')
	return render(request, 'uploader/add-online-news.html',{'form':form, 'medias':medias})

@login_required(login_url='/login/')
def edit_online_news(request, pk):
	instance = TbArticles.objects.get(pk =pk)
	if request.method == 'POST':
		
		form = ArticleOnlineForm(request.POST, instance=instance)
		if form.is_valid():
			article=form.save(commit=False)
			media = TbMedia.objects.get(pk = article.media_id)
			article.rate_bw = media.rate_bw
			article.rate_fc = media.rate_fc
			article.is_chart =0
			article.is_table =0
			article.is_colour=0
			article.size_jpeg=0
			article.columne =0
			article.save()
			elastic = DigivlaElasticArticle()
			elastic.update_article(article)

			return http.HttpResponseRedirect(reverse('list-online-news'))
	else:		
		
		form = ArticleOnlineForm(instance=instance)

	medias = TbMedia.objects.filter(media_type_id=4, statuse='A')

	return render(request, 'uploader/add-online-news.html',{'medias':medias, 'form':form})

@login_required(login_url='/login/')
def delete_online_news(request, pk):
	article = TbArticles.objects.get(pk=pk)
	article.delete()
	elastic = DigivlaElasticArticle()
	elastic.delete_article(article.article_id)
	return http.HttpResponseRedirect(reverse("list-media"))

@login_required(login_url='/login/')
def delete_online_news_confirmation(request, pk):
	article = TbArticles.objects.get(pk=pk)
	return render(request, 'uploader/delete-confirmation.html',{'cancel_link':reverse('list-online-news'),'ok_link':reverse('delete-online-news',kwargs={'pk':pk}), 'object':article})



@login_required(login_url='/login/')
def list_online_news(request):
	row_per_page=50
	page = request.GET.get('page', 1)
	page=int(page)
	term = request.GET.get('term',None)
	sdate = request.GET.get('sdate',None)
	edate = request.GET.get('edate',None)
	media_id = request.GET.get('media_id',None)
	if page <= 0:
		page=1
	
	offset = 1+((page-1)*row_per_page)
	limit = offset+row_per_page
	qs=""
	article_list=None
	print term		

	if term:
		article_id = 0
		
		try:
			article_id=int(term)
		except Exception as e:
			article_id=0

		if article_id > 100:
			article_list = TbArticles.objects.filter(pk=article_id)
			
		elif sdate and edate:
			if media_id:
				article_list =TbArticles.objects.filter(title=term, datee__gte=sdate, datee__lte=edate,media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&term=%s&sdate=%s&edate=%s&media_id=%s" %(term,sdate,edate,media_id)
			else:
				article_list =TbArticles.objects.filter(title=term, datee__gte=sdate, datee__lte=edate).order_by('-pk')[offset:limit]
				qs = "&term=%s&sdate=%s&edate=%s" %(term,sdate,edate)
			print("sdate and edate")
		elif sdate:
			if media_id:
				article_list =TbArticles.objects.filter(title=term, datee__gte=sdate, media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&term=%s&sdate=%s&media_id=%s" %(term,sdate,media_id)
			else:
				article_list =TbArticles.objects.filter(term, datee__gte=sdate).order_by('-pk')[offset:limit]
				qs = "&term=%s&sdate=%s" %(term,sdate)
			print("sdate")
		elif edate:
			if media_id:
				article_list =TbArticles.objects.filter(title=term, datee__lte=edate, media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&term=%s&edate=%s&media_id=%s" %(term,edate,media_id)
			else:
				article_list =TbArticles.objects.filter(title=term, datee__lte=edate).order_by('-pk')[offset:limit]
				qs = "&term=%s&edate=%s" %(term,edate)
		else:
			if media_id:
				article_list =TbArticles.objects.filter(title=term, media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&term=%s&media_id=%s" %(term, media_id)
			else:
				article_list =TbArticles.objects.filter(title=term).order_by('-pk')[offset:limit]
				qs = "&term=%s" %term

	else:
		print("else")
		if sdate and edate:
			if media_id:
				article_list =TbArticles.objects.filter(datee__gte=sdate, datee__lte=edate,media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&sdate=%s&edate=%s&media_id=%s" %(sdate,edate,media_id)
			else:
				article_list =TbArticles.objects.filter(datee__gte=sdate, datee__lte=edate).order_by('-pk')[offset:limit]
				qs = "&sdate=%s&edate=%s" %(sdate,edate)
		elif sdate:
			if media_id:
				article_list =TbArticles.objects.filter(datee__gte=sdate, media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&sdate=%s&media_id=%s" %(sdate,media_id)
			else:
				article_list =TbArticles.objects.filter(datee__gte=sdate).order_by('-pk')[offset:limit]
				qs = "&sdate=%s" %(term,sdate)
		elif edate:
			if media_id:
				article_list =TbArticles.objects.filter(datee__lte=edate, media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&edate=%s&media_id=%s" %(edate,media_id)
			else:
				article_list =TbArticles.objects.filter(datee__lte=edate).order_by('-pk')[offset:limit]
				qs = "&edate=%s" %(edate)
		else:
			if media_id:
				article_list =TbArticles.objects.filter(media_id=media_id).order_by('-pk')[offset:limit]
				qs = "&media_id=%s" %(media_id)
			else:
				# raise Exception("you must provider term or filter item")
				article_list = TbArticles.objects.all().order_by('-pk')[offset:limit]
		# article_list =TbArticles.objects.filter(Q(title__icontains=term)|Q(content__icontains=term)).order_by('-pk')[offset:limit]
		# qs = "&term=%s" %term
		# else:
		# article_list = TbArticles.objects.all().order_by('-pk')[offset:limit]
	
	print(qs)
	medias = TbMedia.objects.filter(media_type_id=4, statuse='A')
	return render(request, 'uploader/list-online-news.html',{'articles':article_list,'page':page,'qs':qs,'medias':medias,'sdate':sdate,'edate':edate,"media_id":media_id})