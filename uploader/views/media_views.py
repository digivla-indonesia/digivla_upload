from django.shortcuts import render
from uploader.models import  TbArticlesTv, TbArticles
from media_models.models import TbMedia, TbMediaType
from django.core.paginator import  Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django import http
from django.contrib.auth.decorators import login_required
from django.db import  transaction
from django.db.models import Q
from django.conf import settings 
from django.core.urlresolvers import reverse

from uploader.forms import ArticleTvForm, LoginForm, ArticleOnlineForm, MediaForm



# Create your views here.




# =========== REGION MEDIA =================
@login_required(login_url='/login/')
def add_media(request,instance=None):
	if request.method == 'POST':
		form = MediaForm(request.POST, instance=instance)
		if form.is_valid():
			media=form.save(commit=False)
			media.usere=1

			media.save()
			return http.HttpResponseRedirect(reverse('list-media'))
	else:
		form = MediaForm(instance=instance)

	media_types = TbMediaType.objects.all()

	return render(request, 'uploader/add-media.html',{'media_types':media_types, 'form':form})

@login_required(login_url='/login/')
def edit_media(request,pk):
	instance = TbMedia.objects.get(media_id =pk)
	if request.method == 'POST':
		
		form = MediaForm(request.POST, instance=instance)
		if form.is_valid():
			media=form.save(commit=False)
			media.usere=1

			media.save()
			return http.HttpResponseRedirect(reverse('list-media'))
	else:		
		
		form = MediaForm(instance=instance)

	media_types = TbMediaType.objects.all()

	return render(request, 'uploader/add-media.html',{'media_types':media_types, 'form':form})

@login_required(login_url='/login/')
def list_media(request, instance=None):
	
	page = request.GET.get('page',1)
	page = int(page)
	term = request.GET.get('term',None)
	sdate = request.GET.get('sdate',None)
	edate = request.GET.get('edate',None)
	media_type_id = request.GET.get('media_type_id',None)
	qs=''
	if term:
		media_id = 0
		
		try:
			 media_id=int(term)
		except Exception as e:
			 media_id=0

		if  media_id > 0:
			list_media = TbMedia.objects.filter(pk=media_id)
			
		elif sdate and edate:
			if media_type_id:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__gte=sdate, input_date__lte=edate,media_type_id=media_type_id).order_by('-pk')
				qs = "&term=%s&sdate=%s&edate=%s&media_type_id=%s" %(term,sdate,edate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__gte=sdate, input_date__lte=edate).order_by('-pk')
				qs = "&term=%s&sdate=%s&edate=%s" %(term,sdate,edate)
			print("sdate and edate")
		elif sdate:
			if media_type_id:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__gte=sdate, media_type_id=media_type_id).order_by('-pk')
				qs = "&term=%s&sdate=%s&media_type_id=%s" %(term,sdate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__gte=sdate).order_by('-pk')
				qs = "&term=%s&sdate=%s" %(term,sdate)
			print("sdate")
		elif edate:
			if media_type_id:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__lte=edate, media_type_id=media_type_id).order_by('-pk') 
				qs = "&term=%s&edate=%s&media_type_id=%s" %(term,edate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(media_name__icontains=term, input_date__lte=edate).order_by('-pk') 
				qs = "&term=%s&edate=%s" %(term,edate)
		else:
			if media_type_id:
				list_media =TbMedia.objects.filter(media_name__icontains=term, media_type_id=media_type_id).order_by('-pk') 
				qs = "&term=%s&media_type_id=%s" %(term, media_type_id)
			else:
				list_media =TbMedia.objects.filter(media_name__icontains=term).order_by('-pk') 
				qs = "&term=%s" %term

	else:
		
		if sdate and edate:
			if media_type_id:
				list_media =TbMedia.objects.filter(input_date__gte=sdate, input_date__lte=edate,media_type_id=media_type_id).order_by('-pk') 
				qs = "&sdate=%s&edate=%s&media_type_id=%s" %(sdate,edate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(input_date__gte=sdate, input_date__lte=edate).order_by('-pk') 
				qs = "&sdate=%s&edate=%s" %(sdate,edate)
		elif sdate:
			if media_type_id:
				list_media =TbMedia.objects.filter(input_date__gte=sdate, media_type_id=media_type_id).order_by('-pk') 
				qs = "&sdate=%s&media_type_id=%s" %(sdate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(input_date__gte=sdate).order_by('-pk') 
				qs = "&sdate=%s" %(term,sdate)
		elif edate:
			if media_type_id:
				list_media =TbMedia.objects.filter(input_date__lte=edate, media_type_id=media_type_id).order_by('-pk') 
				qs = "&edate=%s&media_type_id=%s" %(edate,media_type_id)
			else:
				list_media =TbMedia.objects.filter(input_date__lte=edate).order_by('-pk') 
				qs = "&edate=%s" %(edate)
		else:
			if media_type_id:
				list_media =TbMedia.objects.filter(media_type_id=media_type_id).order_by('-pk') 
				qs = "&media_type_id=%s" %(media_type_id)
			else:
				# raise Exception("you must provider term or filter item")
				list_media = TbMedia.objects.all().order_by('-pk')
		# article_list =TbArticles.objects.filter(Q(media_type__icontains=term)|Q(content__icontains=term)).order_by('-pk') 
		# qs = "&term=%s" %term
		# else:
		# article_list = TbArticles.objects.all().order_by('-pk') 

	paginator = Paginator(list_media,10)
	try:
		medias = paginator.page(page)
	except PageNotAnInteger as e:
		medias = paginator.page(1)
	except EmptyPage:
		medias = paginator.page(paginator.num_pages)
	media_types=TbMediaType.objects.all()
	return render(request, 'uploader/list-media.html',{'medias':medias, 'media_types':media_types,'qs':qs})

@login_required(login_url='/login/')
def delete_media_confirmation(request, pk):
	media = TbMedia.objects.filter(pk=pk).first()
	return render(request, 'uploader/delete-confirmation.html',{'cancel_link':reverse('list-media'),'ok_link':reverse('delete-media',kwargs={'pk':pk}), 'object':media})

@login_required(login_url='/login/')
def delete_media(request,pk):
	media = TbMedia.objects.filter(media_id=pk).first()
	media.delete()
	return http.HttpResponseRedirect(reverse("list-media"))
