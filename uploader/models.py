from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from datetime import datetime
from media_models.models import TbMedia



# Create your models here.
class TbArticles(models.Model):
    article_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=150)
    media_id = models.IntegerField()
    datee = models.DateField()
    content = models.TextField()
    mmcol = models.IntegerField()
    circulation = models.IntegerField()
    page = models.CharField(max_length=10)
    file_pdf = models.CharField(max_length=1024, blank=True, null=True)
    columne = models.IntegerField()
    size_jpeg = models.IntegerField()
    journalist = models.CharField(max_length=120)
    rate_bw = models.IntegerField()
    rate_fc = models.IntegerField()
    is_chart = models.SmallIntegerField()
    is_table = models.SmallIntegerField()
    is_colour = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'tb_articles'

def video_upload_path(media_id,filename):
    media = TbMedia.objects.get(media_id=media_id)
    ext = filename.split('.')[-1]
    if ext != 'mp4':
        raise ValidationError("unsuported format %s just support mp4" %ext)
    now = datetime.now()
    year= now.year
    month = now.month
    day = now.day
    timestamp = now.strftime("%s")
    new_file_name ="%s-%s-%s-%s-%s-%s.%s" %(year,month,day,media.media_type_id,media_id,timestamp,ext)
    return "tv_files/%s/%s/%s/%s" %(year,month,day,new_file_name)


class TbArticlesRadio(models.Model):
    article_id = models.IntegerField(blank=True, null=True)
    media_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    datee = models.DateField(blank=True, null=True)
    journalist = models.CharField(max_length=150, blank=True, null=True)
    timee = models.CharField(max_length=20, blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)
    filee = models.FileField(blank=True, null=True)
    createat = models.DateTimeField(db_column='createAt', auto_now=True)  # Field name made lowercase.


    class Meta:
        managed = False
        db_table = 'tb_articles_radio'

class TbArticlesTv(models.Model):
    article_id = models.IntegerField(blank=True, null=True)
    media_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    datee = models.DateField(blank=True, null=True)
    journalist = models.CharField(max_length=150, blank=True, null=True)
    timee = models.CharField(max_length=20, blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)
    # filee = models.FileField(blank=True, null=True, upload_to='tv_files/%Y/%m/%d')
    
    createat = models.DateTimeField(db_column='createAt',auto_now_add=True)  # Field name made lowercase.

    @deconstructible
    class VideoUploadPath(object):
        def __init__(self):
            pass
        def __call__(self, instance,filename):
            media = TbMedia.objects.get(media_id=instance.media_id)
            ext = filename.split('.')[-1]
            if ext != 'mp4':
                raise ValidationError("unsuported format %s just support mp4" %ext)
            now = datetime.now()
            year= now.year
            month ="%02d" %now.month
            day = "%02d" %now.day
            timestamp = now.strftime("%s")
            new_file_name ="%s-%s-%s-%03d-%s-%s.%s" %(year,month,day,media.media_type_id,instance.media_id,timestamp,ext)
            full_path= "tv_files/%s/%s/%s/%s" %(year,month,day,new_file_name)
            return full_path
    filee = models.FileField(blank=True, null=True, upload_to=VideoUploadPath())
    def __str__(self):
        return self.title


    class Meta:
        managed = False
        db_table = 'tb_articles_tv'