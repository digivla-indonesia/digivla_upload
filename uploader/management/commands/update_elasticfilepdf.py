from django.core.management.base import BaseCommand, CommandError
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError
from django.conf import settings


from uploader.models import TbArticlesTv, TbArticles,TbMedia
import os

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError


class Command(BaseCommand):
	def add_arguments(self, parser):
		pass
	def handle(self, *args, **options):
		tvs = TbArticlesTv.objects.filter(pk__gt=2280)
		es = Elasticsearch([{'host':settings.DIGIVLA_ELASTIC_HOST,'port':settings.DIGIVLA_ELASTIC_PORT,'use_ssl':False},])
		for tv in tvs:
			article = TbArticles.objects.filter(pk=tv.article_id).first()
			if article:
				print("trying update article %s tv id : %s" %(article.article_id, tv.pk))
				body={ 'doc': {"article_id":str(article.article_id),
		                "title" : article.title,
	            		"media_id" : str(tv.media_id),
		                "datee" : article.datee.strftime("%Y-%m-%d %H:%M:%S"),
	             		"content" : article.content,
				 		"mmcol" : str(article.mmcol),
				 		"circulation" : str(article.circulation),
				 		"page" : article.page,
		                "file_pdf" : tv.filee.name.split('/')[-1],
		                "columne" : str(article.columne),
		                "size_jpeg" : str(article.size_jpeg),
		                "journalist" : article.journalist,
		                "rate_bw" : str(article.rate_bw),
		                "rate_fc" : str(article.rate_fc),
		                "is_chart" : str(article.is_chart),
		                "is_table" : str(article.is_table),
		                "is_colour" : str(article.is_colour)
		              }
		             }

				try:
					data =es.update(index='digivlaschema',doc_type="content", id=tv.article_id,body=body)
					

				except NotFoundError as e:
					print("not found")


