from django.core.management.base import BaseCommand, CommandError
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError
from django.conf import settings
from django.db import transaction

from uploader.models import TbArticlesTv, TbArticles,TbMedia
import os



class Command(BaseCommand):
	def add_arguments(self, parser):
		parser.add_argument("--id", help="id media tv target")

	def handle(self, *args, **options):
		if not options['id']:
			raise Exception("id news tv required")

		tv_id = options['id']
		tv=TbArticlesTv.objects.get(pk=tv_id)
		media = TbMedia.objects.get(pk=tv.media_id)
		file = tv.filee.name
		print(file)
		ext = file.split('.')[-1]
		createat = tv.createat
		print(createat)
		year= createat.year
		month ="%02d" %createat.month
		day = "%02d" %createat.day
		timestamp = createat.strftime("%s")
		new_file_name ="%s-%s-%s-%03d-%s-%s.%s" %(year,month,day,media.media_type_id,tv.media_id,timestamp,ext)
		file_path_relative= "tv_files/%s/%s/%s/%s" %(year,month,day,new_file_name)
		# full_path =os.path.join(settings.MEDIA_ROOT, full_path)
		file2rename = os.path.join(settings.MEDIA_ROOT,file)
		print(new_file_name)
		

		with transaction.atomic():
			print("move file from %s to  %s" %(file2rename, os.path.join(settings.MEDIA_ROOT,file_path_relative)))
			tv.filee =file_path_relative
			tv.save()
			article = TbArticles.objects.get(pk=tv.article_id)
			article.file_pdf = tv.filee.name.split('/')[-1]
			article.save()
			os.rename(file2rename,os.path.join(settings.MEDIA_ROOT,file_path_relative))


		
