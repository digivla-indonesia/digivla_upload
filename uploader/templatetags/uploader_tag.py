from django import template

register = template.Library()

@register.inclusion_tag('uploader/tags/tv-form.html')
def tv_forms(form_number, medias):

	return {'form_number':form_number,'medias':medias}