class DefaultRouter(object):
	def db_for_read(self, model, **hincts):
		if model._meta.app_label == 'uploader':
			return 'digivla'
		if model._meta.app_label == 'auth':
			return 'default'
		if model._meta.app_label =='media_models':
			return 'media_models'
		return 'default'

	def db_for_write(self, model, **hints):
		if model._meta.app_label == 'uploader':
			return 'digivla'
		elif model._meta.app_label == 'auth':
			return 'default'
		elif model._meta.app_label =='media_models':
			return 'media_models'
		return None

	def allow_migrate(self, db, app_label, model_name=None, **hints):
		if app_label == 'uploader':
			return False
		elif app_label =='auth':
			return True
		elif app_label == 'media_models':
			return False
		return None

		# def allow_relation(self, obj1, obj2, **hints):
		# 	if obj1._meta.app_label == 'crawler_cms':
		# 		return 'trinix'
		# 	return None

		