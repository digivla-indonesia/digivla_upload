"""trinix_uploader URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views
from uploader.forms import LoginForm

from uploader.views import tv_views, media_views, article_online_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', tv_views.upload_tv),
    url(r'^login/$',auth_views.login,{'template_name': 'uploader/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login'}, name='page-logout'), 
    # url(r'^index/$',views.index),
    url(r'^tv/upload/$',tv_views.upload_tv, name='upload-tv'),
    url(r'^tv/list/$', tv_views.list_tv, name='list-tv'),
    url(r'^tv/(?P<tv_id>\d+)/delete/$', tv_views.delete_tv, name='delete-tv'),
    url(r'^tv/(?P<pk>\d+)/del-confirm/$', tv_views.delete_tv_confirmation, name='delete-tv-confirm'),
    url(r'^tv/(?P<pk>\d+)/edit/$', tv_views.edit_tv, name='edit-tv'),
    url(r'^tv/(?P<article_id>\d+)/detail/$', article_online_views.article_detail, name='article-detail'),

    
    url(r'^online-news/list/$',article_online_views.list_online_news, name='list-online-news'),
    url(r'^online-news/add/$', article_online_views.add_online_news, name='add-online-news'),
    url(r'^online-news/(?P<pk>\d+)/edit/$', article_online_views.edit_online_news, name='edit-online-news'),
    url(r'^online-news/(?P<pk>\d+)/delete/$', article_online_views.delete_online_news, name='delete-online-news'),
    url(r'^online-news/(?P<pk>\d+)/del-confirm/$', media_views.delete_media_confirmation, name='delete-online-news-confirm'),
   
    url(r'^media/add/$', media_views.add_media, name='add-media'),
    url(r'^media/list$', media_views.list_media, name='list-media'),
    url(r'^media/(?P<pk>\d+)/edit/$', media_views.edit_media, name='edit-media'),
    url(r'^media/(?P<pk>\d+)/delete/$', media_views.delete_media, name='delete-media'),
    url(r'^media/(?P<pk>\d+)/del-confirm/$', media_views.delete_media_confirmation, name='delete-media-confirm'),
    # url(r'^progress/', include('progress.urls'))

    
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'Trinix Uploader'