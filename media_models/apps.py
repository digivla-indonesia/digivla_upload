from django.apps import AppConfig


class MediaModelsConfig(AppConfig):
    name = 'media_models'
