from __future__ import unicode_literals

from django.db import models

# Create your models here.
class TbMediaType(models.Model):
    media_type_id = models.SmallIntegerField(primary_key=True)
    media_type = models.CharField(max_length=50)
    media_type_en = models.CharField(max_length=50)
    tool_kits_name = models.CharField(max_length=50)
    is_pdf = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tb_media_type'

class TbMedia(models.Model):
    media_id = models.IntegerField(primary_key=True)
    media_name = models.CharField(max_length=50)
    media_type_id = models.SmallIntegerField()
    circulation = models.IntegerField()
    rate_bw = models.IntegerField()
    rate_fc = models.IntegerField()
    language = models.CharField(max_length=3)
    statuse = models.CharField(max_length=1)
    usere = models.CharField(max_length=20)
    pc_name = models.CharField(max_length=20)
    input_date = models.DateTimeField(auto_now_add=True)
    tier = models.IntegerField(blank=True, null=True)
    @property
    def pretty_status(self):
        if self.statuse == 'N':
            return "Not Active"
        elif self.statuse =='A':
            return "Active"

    class Meta:
        managed = False
        db_table = 'tb_media'

    def __str__(self):
        return self.media_name